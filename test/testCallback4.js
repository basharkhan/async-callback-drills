const boards = require("../data/boards.json");
const lists = require("../data/lists.json");
const cards = require("../data/cards.json");

// finding board id of that board which have name = Thanos in order to get lists related to board id

let boardIdThanos;

for (let index in boards) {
  if (boards[index]["name"] == "Thanos") boardIdThanos = boards[index]["id"];
}

const func1 = require("../callback1");

const func2 = require("../callback2");

const func3 = require("../callback3");

const toGetBoardInfo = require("../callback4");
const toGetList = require("../callback4");
const toGetCards = require("../callback4");

// calling function

const callMe = async () => {
  let infoObj = await toGetBoardInfo(boards, boardIdThanos, func1);
  console.log(infoObj);

  let newArr = await toGetList(lists, boardIdThanos, func2);
  console.log(newArr);

  let listId = [];

  for (let index in newArr) {
    if (newArr[index]["name"] == "Mind") {
      listId.push(newArr[index]["id"]);
    }
  }

  let newCardArr = await toGetCards(cards, listId, func3);
  console.log(newCardArr);
};

callMe();
