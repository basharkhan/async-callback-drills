// Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind and Space lists simultaneously

// Function for board ----------------------------------------------------------------

const toGetBoardInfo = function (boards, boardIdThanos, func1) {
  return new Promise((resolve, reject) => {
    func1(boards, boardIdThanos, (infoObj) => {
      resolve(infoObj);
    });
  });
};

// Function for lists ----------------------------------------------------------------

const toGetList = function (lists, boardIdThanos, func2) {
  return new Promise((resolve, reject) => {
    func2(lists, boardIdThanos, (newArr) => {
      resolve(newArr);
    });
  });
};

// Function for cards ----------------------------------------------------------------

const toGetCards = function (cards, listId, func3) {
  return new Promise((resolve, reject) => {
    func3(cards, listId, (newCardArr) => {
      resolve(newCardArr);
    });
  });
};

module.exports = toGetBoardInfo;
module.exports = toGetList;
module.exports = toGetCards;
