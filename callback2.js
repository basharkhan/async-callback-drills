// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

function boardList(lists, idB, cb) {
  if (typeof lists !== "object") {
    throw new Error(`lists is not valid object`);
  }

  if (idB == undefined || idB == null) {
    throw new Error(`idB is invalid`);
  }

  let newArr = [];

  setTimeout(() => {
    for (let key in lists) {
      if (key == idB) {
        newArr = lists[key];
      }
    }
    cb(newArr);
  }, 2000);
}

module.exports = boardList;
