// Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

function cardsList(cards, listId, cb) {
  if (typeof cards !== "object") {
    throw new Error(`cards is not valid object`);
  }

  if (listId == undefined && listId == null) {
    throw new Error(`listId is not valid`);
  }

  let newArr = [];

  setTimeout(() => {
    for (let key in cards) {
      // if (key == listId) newArr = cards[key];
      if (listId.includes(key)) newArr.push(cards[key]);
    }
    cb(newArr);
  }, 2000);
}

module.exports = cardsList;
