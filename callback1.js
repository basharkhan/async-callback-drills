// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.


function boardInfo(boards, idB, cb){

    if(typeof boards !== "object"){
        throw new Error(`boards is not valid object`)
    }

    if(idB == undefined || idB == null){
         throw new Error(`idB is invalid`);
    }
    
    let infoObj = {};
    
 setTimeout(() => {

    for(let index = 0; index < boards.length; index++){
        if(boards[index]["id"] == idB) {
            infoObj = boards[index];
            break;
        }
    }

    cb(infoObj);
    
}, 2000);
}

module.exports = boardInfo;
